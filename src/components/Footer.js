import React from 'react';

const Footer = () => {
    return (
        <div className="footer">
            Shin-Yuh Wang 2021©
        </div>
    );
};

export default Footer
