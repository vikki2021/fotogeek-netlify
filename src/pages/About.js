import React from "react";

const About = () => {
  return (
    <div className="about" style={{ minHeight: "90vh" }}>
      <h1>About </h1>
      <h2>
        Welcome to #FotoGeek gallery, please feel free to load more photo's,
        search photo's by typing the key words, and download photo's for
        non-profit use.
      </h2>
      <h3>Technology: HTML5/CSS/JS6/React (data fetching, use state, etc)</h3>
    </div>
  );
};

export default About;
